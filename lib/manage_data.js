"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAllData = void 0;
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const nanospinner_1 = require("nanospinner");
const firestore = firebase_admin_1.default.firestore();
const spinner = (0, nanospinner_1.createSpinner)('Deleting data...');
const deleteAllData = (userId) => __awaiter(void 0, void 0, void 0, function* () {
    spinner.start();
    try {
        const journeyRef = firestore.collection('journey').doc(userId);
        const journeyPromise = journeyRef.get().then((snapshot) => {
            if (!snapshot.exists) {
                spinner.error({
                    text: 'Failed to delete journey collection. User not found.'
                });
                return Promise.reject();
            }
            else {
                return journeyRef.delete().then(() => {
                    spinner.success({
                        text: 'Journey collection deleted.'
                    });
                });
            }
        });
        const userRef = firestore.collection('users').doc(userId);
        const userPromise = userRef.get().then((snapshot) => {
            if (!snapshot.exists) {
                spinner.error({
                    text: 'Failed to delete user collection. User not found.'
                });
                return Promise.reject();
            }
            else {
                return userRef.delete().then(() => {
                    spinner.success({
                        text: 'User collection deleted.'
                    });
                });
                ;
            }
        });
        yield Promise.all([
            journeyPromise, userPromise
        ]);
        const weightCol = firestore.collection('weight');
        const snapshots = yield weightCol.where('userId', '==', userId).get();
        snapshots.forEach((snapshot) => __awaiter(void 0, void 0, void 0, function* () {
            yield weightCol.doc(snapshot.id).delete();
        }));
        spinner.success({
            text: 'Weight collection deleted.'
        });
    }
    catch (e) {
        spinner.error({
            text: 'Failed to delete all weight collection'
        });
        spinner.clear();
    }
});
exports.deleteAllData = deleteAllData;
