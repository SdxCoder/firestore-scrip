
import admin from "firebase-admin";
import { createSpinner } from "nanospinner";


const firestore = admin.firestore();
const spinner = createSpinner('Deleting data...');


export const deleteAllData = async (userId: string) => {
    spinner.start()

    try {

        const journeyRef = firestore.collection('journey').doc(userId);
        const journeyPromise = journeyRef.get().then((snapshot) => {
            if (!snapshot.exists) {

                spinner.error({
                    text: 'Failed to delete journey collection. User not found.'
                })

                return Promise.reject()
            }
            else {
                return journeyRef.delete().then(() => {
                    spinner.success({
                        text: 'Journey collection deleted.'
                    })
                });
            }
        })

        const userRef = firestore.collection('users').doc(userId);
        const userPromise = userRef.get().then((snapshot) => {
            if (!snapshot.exists) {

                spinner.error({
                    text: 'Failed to delete user collection. User not found.'
                })

                return Promise.reject()
            }
            else {
                return userRef.delete().then(() => {
                    spinner.success({
                        text: 'User collection deleted.'
                    })
                });;
            }
        })

        await Promise.all([
            journeyPromise, userPromise
        ])

        const weightCol = firestore.collection('weight');
        const snapshots = await weightCol.where('userId', '==', userId).get();

        snapshots.forEach(async snapshot => {
            await weightCol.doc(snapshot.id).delete();
        });

        spinner.success({
            text: 'Weight collection deleted.'
        })

    }
    catch (e) {
        spinner.error({
            text: 'Failed to delete all weight collection'
        })
        spinner.clear()
    }

}