import figlet from "figlet";
import * as inquirer from "inquirer";
import { deleteAllData } from "./manage_data";


export const main = async () => {
  

    console.log("\n");

    console.log(figlet.textSync('Firebase!', {
        horizontalLayout: 'default',
        verticalLayout: 'default',
        width: 80,
        whitespaceBreak: true,
    }));

    console.log("\n");

    const deleteData = await inquirer.prompt({
        name: 'operation',
        type: "list",
        message: "Do you want to delete data by \n",
        choices: [
            "Collection Name", "All"
        ],
       
    })

    console.log("\n");

    const user = await inquirer.prompt({
        name: 'userId',
        type: "input",
        message: "Enter user's id whose data you want to delete",
    })
    
    if (deleteData.operation === 'All') {
        await deleteAllData(user.userId);
       
    }
    else if (deleteData.operation === 'Collection Name') {

    }
}

